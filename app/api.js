import { AsyncStorage } from 'react-native';

const apiKey = '34738023d27013e6d1b995443764da44';
const endPoint = 'https://api.themoviedb.org/3/movie/';
const orderOptions = [
  {label: 'Most Popular', code: 'popular'},
  {label: 'Top Rated', code: 'top_rated'}
]

let api = {
  getOrderOptions() {
    return orderOptions;
  },

  getApiEndPoint(strOrder) {
    return `${endPoint}${strOrder}?api_key=${apiKey}`;
  },

  getUrlPoster(poster_path) {
    return `http://image.tmdb.org/t/p/w500${poster_path}`;
  },

  async getData(urlEndPoint) {
    const response = await fetch(urlEndPoint);
    return response;
  }
};

module.exports = api;
