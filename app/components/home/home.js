import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, Button, TouchableHighlight, Alert} from 'react-native';
import logo from "./../../assets/logo.png";
import {styles} from './styles';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Movies App,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class Home extends Component<Props> {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.container}>
        <Image source={logo} style={styles.imageLogo} />
        <Text style={styles.welcome}>Movies App!</Text>

        <TouchableHighlight
          onPress={()=>this.props.navigation.navigate('Dashboard', {title: 'test title'})}
          underlayColor="white"
        >
          <View style={styles.listButton}>
            <Text style={styles.textButton}>List Movies</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}
