import {StyleSheet} from 'react-native';

const primaryColor = '#01d277';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  imageLogo: {
    height: 200,
    width: 200,
    resizeMode: 'contain'
  },
  listButton: {
    borderRadius: 30,
    backgroundColor: primaryColor
  },
  textButton: {
    color: 'white',
    padding: 10,
    fontWeight: 'bold'
  }
});
