import { StyleSheet } from 'react-native';

const primaryColor = '#01d277';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  titleCardItem: {
    backgroundColor: "#081c24"
  },
  titleThumbnail: {
    borderWidth: 0.8,
    borderColor: primaryColor
  },
  movieTitle: {
    color: primaryColor
  },
  poster: {
    height: 200,
    width: null,
    flex: 1
  },
  headerBar: {
    backgroundColor: primaryColor
  },
  pickerText: {
    color: 'white'
  }
})
