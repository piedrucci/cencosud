import React, { Component } from 'react';
import { Image, View, StyleSheet, ActivityIndicator, Modal, Alert, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, ActionSheet, Picker } from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import {styles} from './styles';
import api from './../../api';
import { Rating } from 'react-native-elements';

export default class CardImageExample extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    movies : [],
    loading: false,
    selected: 1
  };

  componentDidMount() {
    this.getMovies(api.getOrderOptions()[0]['code']);
  }

  getMovies = async(order) => {
    this.setState({loading:true});

    const endPoint = api.getApiEndPoint(order);
    const response = await api.getData(endPoint);
    const responseJson = await response.json();

    this.setState({
      movies: responseJson.results,
      loading: false
    })
  }

  onValueChange = (value) => {
    this.setState({
      selected: value
    });

    this.getMovies(value);
  }

  render() {
    return (
        <Container>
          <Header style={styles.headerBar}>
            <Picker
                note
                mode="dropdown"
                selectedValue={this.state.selected}
                onValueChange={this.onValueChange.bind(this)}
                style={styles.pickerText}
              >
                {
                  api.getOrderOptions().map((option, index) => {
                    return (
                      <Picker.Item key={index} label={option.label.toString()} value={option.code.toString()} />
                    )
                  })
                }
              </Picker>
          </Header>
        <Content>
          {
            (this.state.loading)
            ?<ActivityIndicator size="large" color="#01d277" />
            :this.state.movies.map((movie, index) => {
              const poster = api.getUrlPoster(movie.poster_path);

              return(
                <View key={index}>
                <CardItem style={styles.titleCardItem}>
                  <Left>
                    <Thumbnail style={styles.titleThumbnail} source={{uri: poster}} />
                    <Body>
                      <Text style={styles.movieTitle}>{movie.title}</Text>
                      <Rating imageSize={14} readonly type='custom'
                        ratingColor='#081c24' ratingTextColor='blue' ratingBackgroundColor='white' ratingCount={movie.vote_average} />
                      <Text note>Rate: {movie.vote_average}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image source={{uri: poster}} style={styles.poster}/>
                </CardItem>
                <CardItem>
                  <Text>{movie.overview}</Text>
                </CardItem>
                <CardItem>
                  <Left>
                    <Button transparent>
                      <Icon active name="thumbs-up" />
                      <Text>{movie.vote_count} Votes</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button transparent>
                      <Icon active name="calendar" />
                      <Text>{movie.release_date}</Text>
                    </Button>
                  </Right>
                </CardItem>
                </View>
              )
            })
          }
        </Content>
      </Container>
    );
  }
}
