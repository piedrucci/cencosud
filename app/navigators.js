import { createStackNavigator } from 'react-navigation'

import Home from './components/home/home'
import Dashboard from './components/dashboard/dashboard'

const Navigation = createStackNavigator(
  {
    Home: {screen: Home},
    Dashboard: {screen: Dashboard}
  },
  {
    initialRouteName: 'Home'
  }
);

export default Navigation
